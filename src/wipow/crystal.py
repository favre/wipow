# -*- coding: utf-8 -*-

#   (c) 2022-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

"""Custom crystal widget"""

__all__ = ['CrystalWidget']

import numpy as np
import ipywidgets as widgets
from pyobjcryst.crystal import Crystal
from .utils import NoEventContextManager


class CrystalWidget(widgets.VBox):
    """Widget to display & edit the Crystal name, lattice parameters
    and spacegroup."""

    def __init__(self, crystal: Crystal, parent=None):
        """
        Create the crystal widget.

        :param crystal: the pyobjcryst Crystal object
        :param parent: a parent widget with a update_1d() function to be called
            and update the 1D plot when the lattice or spacegroup changes.
        """
        super(CrystalWidget, self).__init__()
        self.crystal = crystal
        self.parent = parent  # parent
        self.name = widgets.Text(value=crystal.GetName(), description='', tooltip='Name',
                                 layout=widgets.Layout(min_width="10em", max_width="19em"),
                                 continuous_update=False)
        self.formula = widgets.Text(value=crystal.GetFormula(), description='', tooltip='Formula',
                                    layout=widgets.Layout(min_width="10em", max_width="19em"), disabled=True)
        self.color_picker = widgets.ColorPicker(bconcise=False, description='Colour', value='blue',
                                                layout=widgets.Layout(max_width="19em", min_width="5em"))
        self.lattice_abc = widgets.HBox()
        self.lattice_a = widgets.BoundedFloatText(value=crystal.a, description='a',
                                                  min=2, max=100, step=0.005,
                                                  style={'description_width': '1em'},
                                                  layout=widgets.Layout(max_width="6em", min_width="5em"))
        self.lattice_b = widgets.BoundedFloatText(value=crystal.b, description='b',
                                                  min=2, max=100, step=0.005,
                                                  style={'description_width': '1em'},
                                                  layout=widgets.Layout(max_width="6em", min_width="5em"))
        self.lattice_c = widgets.BoundedFloatText(value=crystal.b, description='c',
                                                  min=2, max=100, step=0.005,
                                                  style={'description_width': '1em'},
                                                  layout=widgets.Layout(max_width="6em", min_width="5em"))
        self.lattice_abc.children = [self.lattice_a, self.lattice_b, self.lattice_c]

        self.lattice_angles = widgets.HBox()
        self.lattice_alpha = widgets.BoundedFloatText(value=np.rad2deg(crystal.alpha), description=r'\(\alpha\)',
                                                      min=20, max=160, step=0.2,
                                                      style={'description_width': '1em'},
                                                      layout=widgets.Layout(max_width="6em", min_width="3em"))
        self.lattice_beta = widgets.BoundedFloatText(value=np.rad2deg(crystal.beta), description=r'\(\beta\)',
                                                     min=20, max=160, step=0.2,
                                                     style={'description_width': '1em'},
                                                     layout=widgets.Layout(max_width="6em", min_width="3em"))
        self.lattice_gamma = widgets.BoundedFloatText(value=np.rad2deg(crystal.gamma), description=r'\(\gamma\)',
                                                      min=20, max=160, step=0.2,
                                                      style={'description_width': '1em'},
                                                      layout=widgets.Layout(max_width="6em", min_width="3em"))
        self.lattice_angles.children = [self.lattice_alpha, self.lattice_beta, self.lattice_gamma]

        self.spacegroup = widgets.Text(value=crystal.GetSpaceGroup().GetName(), tooltip='Spacegroup',
                                       layout=widgets.Layout(min_width="10em", max_width="19em"), disabled=False,
                                       continuous_update=False)

        self.children = [self.name, self.formula, self.color_picker, self.lattice_abc, self.lattice_angles,
                         self.spacegroup]

        self.lattice_a.observe(self.on_change_lattice)
        self.lattice_b.observe(self.on_change_lattice)
        self.lattice_c.observe(self.on_change_lattice)
        self.lattice_alpha.observe(self.on_change_lattice)
        self.lattice_beta.observe(self.on_change_lattice)
        self.lattice_gamma.observe(self.on_change_lattice)
        self.lattice_gamma.observe(self.on_change_lattice)

        self.spacegroup.observe(self.on_change_spacegroup)
        self.color_picker.observe(self.on_change_colour)
        self.name.observe(self.on_change_name)
        self.update_lattice()  # This will disable fixed
        self._enable_events = True

    def update_lattice(self):
        with NoEventContextManager(self):
            self.lattice_a.value = self.crystal.a
            self.lattice_b.value = self.crystal.b
            self.lattice_c.value = self.crystal.c
            self.lattice_alpha.value = np.rad2deg(self.crystal.alpha)
            self.lattice_beta.value = np.rad2deg(self.crystal.beta)
            self.lattice_gamma.value = np.rad2deg(self.crystal.gamma)

            # Disable constrained parameters
            # TODO: this should ideally not be necessary, but when the
            # spacegroup is changed, the constraints on the parameters
            # are not updated [because these are defined in UnitCell]
            # Adding UnitCell::ChangeSpaceGroup would solve this
            spg = self.crystal.GetSpaceGroup()
            num = spg.GetSpaceGroupNumber()
            a, b, c, alpha, beta, gamma = True, True, True, True, True, True
            if num <= 2:
                pass
            elif (num <= 15) and (0 == spg.GetUniqueAxis()):
                beta = False
                gamma = False
            elif (num <= 15) and (1 == spg.GetUniqueAxis()):
                alpha = False
                gamma = False
            elif (num <= 15) and (2 == spg.GetUniqueAxis()):
                alpha = False
                beta = False
            elif num <= 74:
                alpha = False
                beta = False
                gamma = False
            elif num <= 142:
                b = False
                alpha = False
                beta = False
                gamma = False
            elif spg.GetExtension() == 'R':
                # Rhomboedral
                b = False
                c = False
                alpha = True
                beta = False
                gamma = False
            elif (num <= 194):
                # Hexagonal axes, for hexagonal and non-rhomboedral trigonal cells
                b = False
                alpha = False
                beta = False
                gamma = False
            else:
                b = False
                c = False
                alpha = False
                beta = False
                gamma = False

            if a:
                self.lattice_a.disabled = False
            else:
                self.lattice_a.disabled = True

            if b:
                self.lattice_b.disabled = False
            else:
                self.lattice_b.disabled = True

            if c:
                self.lattice_c.disabled = False
            else:
                self.lattice_c.disabled = True
            if alpha:
                self.lattice_alpha.disabled = False
            else:
                self.lattice_alpha.disabled = True

            if beta:
                self.lattice_beta.disabled = False
            else:
                self.lattice_beta.disabled = True

            if gamma:
                self.lattice_gamma.disabled = False
            else:
                self.lattice_gamma.disabled = True

            if self.parent is not None:
                self.parent.update_1d()
                self.parent.update_powder_pattern_plot()

    def on_change_lattice(self, v=None):
        if v is not None and self._enable_events:
            if v['name'] == 'value':
                if v['owner'].description == 'a':
                    self.crystal.a = self.lattice_a.value
                elif v['owner'].description == 'b':
                    self.crystal.b = self.lattice_b.value
                elif v['owner'].description == 'c':
                    self.crystal.c = self.lattice_c.value
                elif 'alpha' in v['owner'].description:
                    self.crystal.alpha = np.deg2rad(self.lattice_alpha.value)
                elif 'beta' in v['owner'].description:
                    self.crystal.beta = np.deg2rad(self.lattice_beta.value)
                elif 'gamma' in v['owner'].description:
                    self.crystal.gamma = np.deg2rad(self.lattice_gamma.value)
                self.update_lattice()

    def on_change_spacegroup(self, v=None):
        if v is not None and self._enable_events:
            if v['name'] == 'value':
                with NoEventContextManager(self):
                    try:
                        self.crystal.GetSpaceGroup().ChangeSpaceGroup(self.spacegroup.value)
                    except ValueError:
                        pass
                    # In case the spg is not understood
                    self.spacegroup.value = self.crystal.GetSpaceGroup().GetName()

                    # KLUDGE - we need the lattice parameter to change else the constraints won't be updated
                    self.crystal.a += 0.001
                    self.crystal.a -= 0.001

                    self.update_lattice()

    def on_change_colour(self, v=None):
        if v is not None and self.parent is not None:
            if v['name'] == 'value':
                self.parent.update_1d()
                self.parent.update_powder_pattern_plot()

    def on_change_name(self, v=None):
        if v is not None and self.parent is not None:
            if v['name'] == 'value':
                self.crystal.SetName(self.name.value)
                self.parent.update_1d()
