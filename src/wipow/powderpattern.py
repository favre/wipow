# -*- coding: utf-8 -*-

#   (c) 2022-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

"""
Temporary code used to patch PowderPattern for pyobjcryst<2.2.5, until
we find a proper way to deploy a recent version on windows.
See https://github.com/diffpy/pyobjcryst/issues/33
"""

__all__ = ['PowderPattern']

from pyobjcryst.version import __version__

need_patch = True
try:
    v = [int(s) for s in __version__.split('.')]
    if v[0] > 2:
        need_patch = False
    elif v[0] == 2 and v[1] > 2:
        need_patch = False
    elif v[2] >= 5:
        need_patch = False
except Exception:
    pass

if need_patch:
    import warnings
    import numpy as np
    from pyobjcryst.powderpattern import PowderPattern as PowderPattern_orig
    from pyobjcryst._pyobjcryst import PowderPatternDiffraction
    from pyobjcryst import ObjCrystException

    warnings.warn("pyobjcryst>=2.2.5 unavailable, patching PowderPattern for plotting", stacklevel=2)

    # Copy from pyobjcryst 2.2.5
    class PowderPattern(PowderPattern_orig):
        def plot(self, diff=None, hkl=None, figsize=(9, 4), fontsize_hkl=6, reset=False, **kwargs):
            """
            Show the powder pattern in a plot using matplotlib
            :param diff: if True, also show the difference plot
            :param hkl: if True, print the hkl values
            :param figsize: the figure size passed to matplotlib
            :param fontsize_hkl: fontsize for hkl coordinates
            :param reset: if True, will reset the x and y limits, and remove the flags to plot
                          the difference and hkl unless the options are set at the same time.
            :param kwargs: additional keyword arguments:
                           fig=None will force creating a new figure
                           fig=fig1 will plot in the given matplotlib figure
            """
            import matplotlib.pyplot as plt
            obs = self.GetPowderPatternObs()
            try:
                calc = self.GetPowderPatternCalc()
            except ObjCrystException:
                # TODO: when importing  objects from an XML file, the powder pattern does not compute
                #  correctly, Prepare() needs to be called manually. Why ?
                self.Prepare()
                calc = self.GetPowderPatternCalc()

            if reset:
                self._plot_ylim = None
                self._plot_xlim = None
                self._plot_hkl = False
                self._plot_diff = False
            if 'fig' in kwargs:
                self._plot_fig = kwargs['fig']

            if diff is not None:
                self._plot_diff = diff
            plot_diff = self._plot_diff
            if hkl is not None:
                self._plot_hkl = hkl
            plot_hkl = self._plot_hkl

            # TODO: handle other coordinates than angles (TOF)
            x = np.rad2deg(self.GetPowderPatternX())
            if 'inline' not in plt.get_backend():
                if self._plot_fig is None:
                    self._plot_fig = plt.figure(figsize=figsize)
                elif plt.fignum_exists(self._plot_fig.number) is False:
                    # Somehow the figure disappeared, create a new one
                    self._plot_fig = plt.figure(figsize=figsize)
                else:
                    self._plot_fig = plt.figure(self._plot_fig.number)
                plt.clf()
            else:
                plt.figure(figsize=figsize)
            plt.plot(x, obs, 'k', label='obs', linewidth=1)
            plt.plot(x, calc, 'r', label='calc', linewidth=1)
            m = min(1, self.GetMaxSinThetaOvLambda() * self.GetWavelength())
            mtth = np.rad2deg(np.arcsin(m)) * 2
            if plot_diff:
                diff = calc - obs - obs.max() / 20
                # Mask difference above max sin(theta)/lambda
                diff = np.ma.masked_array(diff, x > mtth)
                plt.plot(x, diff, 'g', label='calc-obs',
                         linewidth=0.5)

            plt.legend(loc='upper right')
            if self.GetName() != "":
                plt.title("PowderPattern: %s" % self.GetName())

            if self._plot_ylim is not None:
                plt.ylim(self._plot_ylim)
            if self._plot_xlim is not None:
                plt.xlim(self._plot_xlim)
            elif m < 1:
                plt.xlim(x.min(), mtth)

            if plot_hkl:
                self._do_plot_hkl(nb_max=100, fontsize_hkl=fontsize_hkl)

            # print PowderPatternDiffraction names
            self._plot_phase_labels = []
            iphase = 0
            for i in range(self.GetNbPowderPatternComponent()):
                s = ""
                comp = self.GetPowderPatternComponent(i)
                if comp.GetClassName() == "PowderPatternDiffraction":
                    if comp.GetName() != "":
                        s += "%s\n" % comp.GetName()
                    else:
                        c = comp.GetCrystal()
                        if c.GetName() != "":
                            s += c.GetName()
                        else:
                            s += c.GetFormula()
                        s += "[%s]" % str(c.GetSpaceGroup())
                    if comp.GetExtractionMode():
                        s += "[Le Bail mode]"
                    self._plot_phase_labels.append(s)
                    plt.text(0.005, 0.995, "\n" * iphase + s, horizontalalignment="left", verticalalignment="top",
                             transform=plt.gca().transAxes, fontsize=6, color=self._colour_phases[iphase])
                    iphase += 1

            if 'inline' not in plt.get_backend():
                try:
                    # Force immediate display. Not supported on all backends (e.g. nbagg)
                    plt.draw()
                    plt.gcf().canvas.draw()
                    if 'ipympl' not in plt.get_backend():
                        plt.pause(.001)
                except:
                    pass
                # plt.gca().callbacks.connect('xlim_changed', self._on_xlims_change)
                # plt.gca().callbacks.connect('ylim_changed', self._on_ylims_change)
                self._plot_fig.canvas.mpl_connect('button_press_event', self._on_mouse_event)
                self._plot_fig.canvas.mpl_connect('draw_event', self._on_draw_event)

        def _do_plot_hkl(self, nb_max=100, fontsize_hkl=None):
            import matplotlib.pyplot as plt
            if fontsize_hkl is None:
                fontsize_hkl = self._plot_hkl_fontsize
            else:
                self._plot_hkl_fontsize = fontsize_hkl
            # Plot up to nb_max hkl reflections
            obs = self.GetPowderPatternObs()
            calc = self.GetPowderPatternCalc()
            x = np.rad2deg(self.GetPowderPatternX())
            # Clear previous text (assumes only hkl were printed)
            plt.gca().texts.clear()
            iphase = 0
            for ic in range(self.GetNbPowderPatternComponent()):
                c = self.GetPowderPatternComponent(ic)
                if isinstance(c, PowderPatternDiffraction) is False:
                    continue
                # print("HKL for:", c.GetName())
                xmin, xmax = plt.xlim()
                vh = np.round(c.GetH()).astype(np.int16)
                vk = np.round(c.GetK()).astype(np.int16)
                vl = np.round(c.GetL()).astype(np.int16)
                stol = c.GetSinThetaOverLambda()

                if 'inline' not in plt.get_backend():
                    # 'inline' backend triggers a delayed exception (?)
                    try:
                        # need the renderer to avoid text overlap
                        renderer = plt.gcf().canvas.renderer
                    except:
                        # Force immediate display. Not supported on all backends (e.g. nbagg)
                        plt.draw()
                        plt.gcf().canvas.draw()
                        if 'ipympl' not in plt.get_backend():
                            plt.pause(.001)
                        try:
                            renderer = plt.gcf().canvas.renderer
                        except:
                            renderer = None
                else:
                    renderer = None

                props = {'ha': 'center', 'va': 'bottom'}
                ct = 0
                last_bbox = None
                ax = plt.gca()
                tdi = ax.transData.inverted()
                for i in range(len(vh)):
                    xhkl = np.rad2deg(self.X2XCorr(self.STOL2X(stol[i])))
                    idxhkl = int(round(self.X2PixelCorr(self.STOL2X(stol[i]))))
                    # print(vh[i], vk[i], vl[i], xhkl, idxhkl)
                    if xhkl < xmin or idxhkl < 0:
                        continue
                    if xhkl > xmax or idxhkl >= len(x):
                        break
                    ct += 1
                    if ct >= nb_max:
                        break

                    ihkl = max(calc[idxhkl], obs[idxhkl])
                    s = " %d %d %d" % (vh[i], vk[i], vl[i])
                    t = plt.text(xhkl, ihkl, s, props, rotation=90, fontsize=fontsize_hkl,
                                 fontweight='light', color=self._colour_phases[iphase])
                    if renderer is not None:
                        # Check for overlap with previous
                        bbox = t.get_window_extent(renderer)
                        # print(s, bbox)
                        if last_bbox is not None:
                            if bbox.overlaps(last_bbox):
                                b = bbox.transformed(tdi)
                                t.set_y(ihkl + b.height * 1.2)
                        last_bbox = t.get_window_extent(renderer)
                iphase += 1
            self._last_hkl_plot_xlim = plt.xlim()
            if self._plot_phase_labels is not None:
                for iphase in range(len(self._plot_phase_labels)):
                    s = self._plot_phase_labels[iphase]
                    plt.text(0.005, 0.995, "\n" * iphase + s, horizontalalignment="left", verticalalignment="top",
                             transform=plt.gca().transAxes, fontsize=6, color=self._colour_phases[iphase])

else:
    from pyobjcryst.powderpattern import PowderPattern
